<?php
require 'vendor/autoload.php';

use \unToitPourTous\models\DBConnection;
use \unToitPourTous\controller\ControllerCatalogueUtilisateur;
use \unToitPourTous\controller\ControllerCatalogueLogements;
use \unToitPourTous\controller\ControllerConnection;

DBConnection::getInstance();
session_start();
$app=new Slim\Slim();

$app->get('/',function(){})->name('css');
$app->get('/img/',function(){})->name('img');

$app->get('/', function(){
	
})->name('racine');

$app->get('/utilisateurs',function(){
	(new ControllerCatalogueUtilisateur())->afficherCatalogue();
})->name("utilisateurs");
$app->get('/utilisateur/:id',function($id){
	(new ControllerCatalogueUtilisateur())->afficherUtilisateur($id);
})->name('utilisateur');

$app->get('/logements',function(){
	(new ControllerCatalogueLogements)->afficherCatalogue();
});
$app->get('/logement/:id',function($id){
	(new ControllerCatalogueLogements())->afficherLogement($id);
})->name('logement');

$app->get('/connection',function(){
	(new ControllerConnection())->seConnecter();
});
$app->get('/connection/:id',function($id){
	(new ControllerConnection())->connecterCompte($id);
})->name('connection');

$app->run();
