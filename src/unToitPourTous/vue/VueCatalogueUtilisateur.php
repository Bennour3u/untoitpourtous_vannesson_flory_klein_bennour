<?php

namespace unToitPourTous\vue;


class VueCatalogueUtilisateur
{

	const LISTE_UTILISATEURS=0;
	const PAGE_UTILISATEUR=1;
	private $tab;
	function __construct($liste)
	{
		$this->tab=$liste;
	}

	private function utilisateurToListeHtml($utilisateur)
	{
		$app=\Slim\Slim::getInstance();
		$res='<a href="'.$app->urlFor('utilisateur',array('id' => $utilisateur['id'])).'"">Nom: '.$utilisateur['nom'].'</a><img src="'.$utilisateur['image'].'">';
		return $res;
	}

	private function pageUtilisateurToHtml()
	{
		$res='<html lang="en">
	<head>
  		<title>Bootstrap Example</title>
  		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="./../bootstrap-3.3.7-dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
<style>
.container {
    display: flex; 
    text-align: center;
margin-left: 40%;
}

</style>	
</head>
	<ul class="nav nav-pills">
 		<li role="presentation" class="active"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/connection">Home</a></li>
  		<li role="presentation"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/utilisateurs">Utilisateurs</a></li>
  		<li role="presentation"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/logements">Appartements</a></li>
	</ul>

	<body>
<div class="jumbotron text-center">
  		<h1>Un ToitPourTous</h1>
  		<p>Utilisateur: '.$this->tab['nom'].'</p> 
	</div><div class="container"><div><p><b>Nom:</b> '.$this->tab['nom'].'</p><p><b>Message:</b> '.$this->tab['message'].'</p><p><img src="../'.$this->tab['image'].'"class="img-thumbnail" alt="Cinqe Terre"></div>';
		return $res;
	}
	private function listeUtilisateurToHtml()
	{
		$res='<html lang="en">
	<head>
  		<title>Bootstrap Example</title>
  		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap.min.css">
		<style> 
.well{
    opacity:0.90
}
.fixed-bg {
    background-image: url("oldppl.jpg");
    min-height: 500px;
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    opacity: 0.80;
}
</style>
	</head>
	<ul class="nav nav-pills">
 		<li role="presentation" class="active"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/connection">Home</a></li>
  		<li role="presentation"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/utilisateurs">Utilisateurs</a></li>
  		<li role="presentation"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/logements">Appartements</a></li>
	</ul>

	<body>
	<div class="jumbotron text-center">
  		<h1>Un ToitPourTous</h1>
  		<p>Liste des utilisateurs</p> 
	</div>
	<ul><div class="fixed-bg">';
		foreach ($this->tab as $key => $value) {
			$res.='<li class="well">'.$this->utilisateurToListeHtml($value).'</li>';
		}
		$res.='</ul></div>';
		return $res;

	}

	public function render($valeur)
	{
		$html='';
		
		$res;
		switch ($valeur) {
			case self::LISTE_UTILISATEURS:
				$res=$this->listeUtilisateurToHtml();
				break;
			
			case self::PAGE_UTILISATEUR:
				$res=$this->pageUtilisateurToHtml();
				break;
			default:
				# code...
				break;
		}
		$html=$html.$res;
		$html=$html.'</body>
			</html>';
		echo($html);
	}
}
