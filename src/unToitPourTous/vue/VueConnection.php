<?php

namespace unToitPourTous\vue;

class VueConnection{

	private $tab;
	function __construct($liste)
	{
		$this->tab=$liste;
	}

	private function comptesToListeHtml($utilisateur)
	{
		$app=\Slim\Slim::getInstance();
		$res='<a href="'.$app->urlFor('connection',array('id' => $utilisateur['id'])).'"">Compte de '.$utilisateur['nom'].'</a><img src="'.$utilisateur['image'].'">';
		return $res;
	}

	private function listeComptesToHtml()
	{
		$res='
	
	<ul>';
		foreach ($this->tab as $key => $value) {
			$res.='<li class="well">'.$this->comptesToListeHtml($value).'</li>';
		}
		$res.='</ul>';
		return $res;
	}

	public function render()
	{
		$html='<html lang="en">
	<head>
  		<title>Bootstrap Example</title>
  		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
<style>
.well{
    opacity:0.90
}
.fixed-bg {
    background-image: url("untoitpourtous.jpg");
    min-height: 500px;
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    opacity: 0.80;
}
</style>
	</head>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/connection">Home</a></li>
<li role="presentation"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/utilisateurs">Utilisateurs</a></li>
  		<li role="presentation"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/logements">Appartements</a></li>
  </ul><div class="jumbotron text-center">
  		<h1>Un ToitPourTous</h1>
  		<p>Choisir un compte</p>
	</div>
<ul><div class="fixed-bg">;
	<body>';
		$res=$this->listeComptesToHtml();
		$html=$html.$res;
		$html=$html.'</div</ul><</body>
			</html>';
		echo($html);
	}
}
