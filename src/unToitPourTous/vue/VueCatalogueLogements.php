<?php

namespace unToitPourTous\vue;

class VueCatalogueLogements{


	const LISTE_LOGEMENTS=0;
	const PAGE_LOGEMENT=1;

	private $tab;
	function __construct($liste)
	{
		$this->tab=$liste;
	}

	private function listeLogementsToHtml()
	{
		$res="<ul>";
		foreach ($this->tab as $key => $value) {
			$res.='<li class="well">'.$this->logementToListeHtml($value).'</li>';
		}
		return $res;
	}

	private function logementToListeHtml($value)
	{
		$app=\Slim\Slim::getInstance();
		$res='<a href="'.$app->urlFor('logement',array('id'=>$value['id'])).'">Logement n°'.$value['id'].' </a><img src="'.$value['image'].'">';
		return $res;
	}


	private function pageLogementToHtml()
	{
		$res='<p><b>Logement n°:</b> '.$this->tab['id'].'</p><br><img src="../'.$this->tab['image'].'"><br><p><b>Nombre de places:</b> '.$this->tab['places'].'</p>';
		return $res;
	}
	public function render($value)
	{
		$html='<html lang="en">
	<head>
  		<title>Bootstrap Example</title>
  		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
<style>
.well{
    opacity:0.95
}
.fixed-bg {
    background-image: url("maison2.jpg");
    min-height: 500px;
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    opacity: 0.80;
}
</style>
	</head>
	<ul class="nav nav-pills">
	<li role="presentation" class="active"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/connection">Home</a></li>
  		<li role="presentation"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/utilisateurs">Utilisateurs</a></li>
  		<li role="presentation"><a href="https://webetu.iutnc.univ-lorraine.fr/www/vannesso4u/UnToitPourTous/logements">Appartements</a></li>
<a name="top"></a>
	</ul>
<div class="jumbotron text-center">
  		<h1>Un ToitPourTous</h1>
<p> Logements </p>
  	</div><ul><div class="fixed-bg">';

    
		$res;
		switch ($value) {
			case self::LISTE_LOGEMENTS:
				$res=$this->listeLogementsToHtml();
				break;
			case self::PAGE_LOGEMENT:
				$res=$this->pageLogementToHtml();
				break;
			default:
				
				break;
		}
		$html=$html.$res.'<a href="#top">Retour vers le haut de la page.</a></button></div></ul>';
		echo($html);
	}
}
