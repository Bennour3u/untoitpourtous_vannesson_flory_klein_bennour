<?php

namespace unToitPourTous\controller;

use \unToitPourTous\models\DBConnection;
use \unToitPourTous\models\Utilisateur;
use \unToitPourTous\vue\VueCatalogueUtilisateur;

class ControllerCatalogueUtilisateur {
	
	public function afficherCatalogue()
	{
		$app=\Slim\Slim::getInstance();
		DBConnection::getInstance();
		$listUtilisateurs=Utilisateur::get()->toArray();
		$vue=new VueCatalogueUtilisateur($listUtilisateurs);
		$vue->render(VueCatalogueUtilisateur::LISTE_UTILISATEURS);
	}
	
	public function afficherUtilisateur($id)
	{
		$app=\Slim\Slim::getInstance();
		DBConnection::getInstance();
		$util=Utilisateur::where('id','=',$id)->first()->toArray();
		$vue=new VueCatalogueUtilisateur($util);
		$vue->render(VueCatalogueUtilisateur::PAGE_UTILISATEUR);
	}
	
	
}