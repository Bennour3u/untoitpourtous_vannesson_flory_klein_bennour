<?php

namespace unToitPourTous\controller;

use \unToitPourTous\models\DBConnection;
use \unToitPourTous\models\Logement;
use \unToitPourTous\vue\VueCatalogueLogements;

class ControllerCatalogueLogements {

	public function afficherCatalogue()
	{
		$app=\Slim\Slim::getInstance();
		DBConnection::getInstance();
		$listLogement=Logement::where('places','>',0)->get()->toArray();
		$vue=new VueCatalogueLogements($listLogement);
		$vue->render(VueCatalogueLogements::LISTE_LOGEMENTS);
	}

	public function afficherLogement($id)
	{
		$app=\Slim\Slim::getInstance();
		DBConnection::getInstance();
		$listLogement=Logement::where('id','=',$id)->first()->toArray();
		$vue=new VueCatalogueLogements($listLogement);
		$vue->render(VueCatalogueLogements::PAGE_LOGEMENT);
	}
}