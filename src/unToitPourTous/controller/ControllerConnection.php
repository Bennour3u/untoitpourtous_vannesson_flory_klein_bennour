<?php

namespace unToitPourTous\controller;

use \unToitPourTous\models\DBConnection;
use \unToitPourTous\models\Utilisateur;
use \unToitPourTous\vue\VueConnection;

class ControllerConnection{


	public function seConnecter()
	{
		$app=\Slim\Slim::getInstance();
		DBConnection::getInstance();
		$listUtilisateurs=Utilisateur::get()->toArray();
		$vue=new VueConnection($listUtilisateurs);
		$vue->render();
	}

	public function connecterCompte($id)
	{
		$app=\Slim\Slim::getInstance();
		$_SESSION['idDeSession']=$id;
		$app->redirect($app->urlFor("utilisateurs"));
	}
}