
/*!40101 SET NAMES utf8 */;

CREATE TABLE IF NOT EXISTS `logement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `places` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;


INSERT INTO `logement` (`id`, `places`, `image`) VALUES
(1, 3, 'img/apart/1.jpg'),
(2, 3, 'img/apart/2.jpg'),
(3, 4, 'img/apart/3.jpg'),
(4, 4, 'img/apart/4.jpg'),
(5, 4, 'img/apart/5.jpg'),
(6, 4, 'img/apart/6.jpg'),
(7, 4, 'img/apart/7.jpg'),
(8, 5, 'img/apart/8.jpg'),
(9, 5, 'img/apart/9.jpg'),
(10, 6, 'img/apart/10.jpg'),
(11, 6, 'img/apart/11.jpg'),
(12, 6, 'img/apart/12.jpg'),
(13, 7, 'img/apart/13.jpg'),
(14, 7, 'img/apart/14.jpg'),
(15, 8, 'img/apart/15.jpg'),
(16, 2, 'img/apart/16.jpg'),
(17, 2, 'img/apart/17.jpg'),
(18, 2, 'img/apart/18.jpg'),
(19, 2, 'img/apart/19.jpg'),
(20, 2, 'img/apart/20.jpg'),
(21, 3, 'img/apart/21.jpg'),
(22, 3, 'img/apart/22.jpg'),
(23, 3, 'img/apart/23.jpg'),
(24, 3, 'img/apart/24.jpg'),
(25, 3, 'img/apart/25.jpg');


CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;


INSERT INTO `user` (`id`, `nom`, `message`, `image`) VALUES
(1, 'Jeanne', 'aime la musique ♫', 'img/user/1.jpg'),
(2, 'Paul', 'aime cuisiner ♨ ♪', 'img/user/2.jpg'),
(3, 'Myriam', 'mange Halal ☪', 'img/user/3.jpg'),
(4, 'Nicolas', 'ouvert à tous ⛄', 'img/user/4.jpg'),
(5, 'Sophie', 'aime sortir ♛', 'img/user/5.jpg'),
(6, 'Karim', 'aime le soleil ☀', 'img/user/6.jpg'),
(7, 'Julie', 'apprécie le calme ☕', 'img/user/7.jpg'),
(8, 'Etienne', 'accepte jeunes et vieux ☯', 'img/user/8.jpg'),
(9, 'Max', 'féru de musique moderne ☮', 'img/user/9.jpg'),
(10, 'Sabrina', 'aime les repas en commun ⛵☻', 'img/user/10.jpg'),
(11, 'Nathalie', 'bricoleuse ⛽', 'img/user/11.jpg'),
(12, 'Martin', 'sportif ☘ ⚽ ⚾ ⛳', 'img/user/12.jpg'),
(13, 'Manon', '', 'img/user/13.jpg'),
(14, 'Thomas', '', 'img/user/14.jpg'),
(15, 'Léa', '', 'img/user/15.jpg'),
(16, 'Alexandre', '', 'img/user/16.jpg'),
(17, 'Camille', '', 'img/user/17.jpg'),
(18, 'Quentin', '', 'img/user/18.jpg'),
(19, 'Marie', '', 'img/user/19.jpg'),
(20, 'Antoine', '', 'img/user/20.jpg'),
(21, 'Laura', '', 'img/user/21.jpg'),
(22, 'Julien', '', 'img/user/22.jpg'),
(23, 'Pauline', '', 'img/user/23.jpg'),
(24, 'Lucas', '', 'img/user/24.jpg'),
(25, 'Sarah', '', 'img/user/25.jpg'),
(26, 'Romain', '', 'img/user/26.jpg'),
(27, 'Mathilde', '', 'img/user/27.jpg'),
(28, 'Florian', '', 'img/user/28.jpg');

